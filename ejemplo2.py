if __name__ == "__main__":
    micarro = {
            "marca" : "Lada",
            "modelo" : "Samara",
            "ano"   : 1992,
            "extras" : ["sunroof", "a/c"]
    }
    print(micarro)

    print(micarro["modelo"])
    micarro["ano"] = 2020
    print(micarro)

    if "modelo" in micarro:
        print("Si, este modelo es uno de los principales")
    
    print(len(micarro))
    micarro["color"] = "negro"
    micarro.pop("ano")
    print(micarro)

    if len(micarro["extras"]) > 5:
        print("Full Extras")
    elif len(micarro["extras"]) > 3:
        print("Economico")
    else:
        print("Bicicleta con motor")





