if __name__ == "__main__":
    print("To Do List")

    milista = []

    while True:
        print("\n1. Mostrar tareas")
        print("2. Agregar tareas")
        print("3. Eliminar tareas")
        print("4. Salir")
        try:
            opcion = int(input("Opcion > "))
        except:
            opcion = -1

        if opcion == 1:
            if len(milista) > 0:
                print(milista)
            else:
                print("Esta lista esta vacia...")
        elif opcion == 2:
            t = input("Nueva Tarea: ")
            milista.append(t)
            print("Tarea agregada...")
        elif opcion == 3:
            if len(milista) > 0:
                i = 1
                for tarea in milista:
                    print(str(i) + ". " + tarea)
                    i += 1
                try:
                    te = int(input("Tarea a Eliminar: "))
                except:
                    te = -1
                if te <= len(milista) and te > 0:
                    del milista[te - 1]
                    print("Tarea eliminada")
                else:
                    print("ERROR::Tarea no valida")
            else:
                print("Nada que eliminar...")
        elif opcion == 4:
            break
        else:
            print("ERROR::Invalida")

    print("Ciao que te vi!")
