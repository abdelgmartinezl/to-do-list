if __name__ == "__main__":
    print("Colecciones")

    milista = ["manzana", "guineo", "cereza"]
    print(milista)

    print(milista[1])
    print(milista[-1])
    print(milista[2:4])

    for fruta in milista:
        print("Estoy comiendo un(a) " + fruta)

    if "manzana" in milista:
        print("Hay una manzana en mi camino")

    del milista[2]
    print(milista)

    milista.clear()
    print(milista)

    milista2 = ["cebolla", "apio", "remolacha"]
    del milista
    milista = list(milista2)
    print(milista)

    milista3 = ["pepino", "berenjena", "lechuga"]
    milista = milista2 + milista3
    print(milista)

    x = input("Nombre de Vegetal: ")
    milista.append(x)
    print(milista)


    if len(milista) > 0:
        print("Tiene algo")
    else:
        print("No tiene nada")
